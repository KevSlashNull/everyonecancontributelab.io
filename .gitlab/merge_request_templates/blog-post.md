### Checklist for writer

- [ ] Link to issue added, and set to close when this MR is merged (with `Closes` keyword)
- [ ] Due date and milestone (if applicable) added for the desired publish date
- [ ] All relevant [frontmatter](https://about.gitlab.com/handbook/marketing/blog/#frontmatter) included
- [ ] Reviewed by fellow team member (except for weeekly meeting summary)

/label ~"topic::blogpost"
