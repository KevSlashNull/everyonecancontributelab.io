---
Title: "Handbook"
Date: 2021-02-23
Authors: ["dnsmichi"]
mermaid: true
---

## Introduction

This is the `#everyonecancontribute cafe` handbook with insights, infrastructure documentation and more. 

## Infrastructure

### Website

This website is built with [Hugo](https://gohugo.io/) and deployed with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). Blog posts and page updates happen as merge requests and are deployed with CI/CD pipelines.

`everyonecancontribute.com` is owned by Michael Friedrich, who also manages the DNS records and GitLab Pages challenge. `everyonecancontribute.dev` is available for future demos and experiments.

### Blog posts

This is described in the project's [readme](https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io#how-to-add-content).

## Cafe chats

Michael hosts the weekly coffee chats using his GitLab Zoom account as part of [Developer Evangelism projects](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/), and live streams the session to GitLab Unfiltered on YouTube (see menu header).

Common tasks during the session:

- Monitor the livestream for comments
- Create a Twitter thread from learned insights
- Write the blog post, add the livestream URL

## Social

### Twitter

Michael maintains a Twitter list of all members who have a public account, this is linked in the website menu as single source of truth (SSoT).

## Community

### Discord

> Note: We have tried Twitter DM groups, Telegram groups and Gitter channels before. Discord is the next iteration. 

Server: https://discord.gg/HJPxUGdd9F 

#### Permissions

The `everyonecancontribute` server has the following roles:

- `admin` for group founders
- `everyone` for everyone else

#### Integrations

##### Bots 

TBD.

##### GitLab Notifications

The Discord server has configured webhooks for GitLab groups following the [documentation](https://docs.gitlab.com/ee/user/project/integrations/discord_notifications.html):

| GitLab Project | Channel  | Enabled |
|----------------|----------|---------|
| everyonecancontribute/general | `#cafe` | [Setting](https://gitlab.com/everyonecancontribute/general/-/services/discord/edit) |
| everyonecancontribute/everyonecancontribute.gitlab.io | `#cafe` | [Setting](https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io/-/services/discord/edit) |

| GitLab Group   | Channel  | Enabled |
|----------------|----------|---------|
| everyonecancontribute/keptn | `#keptn` | - |
| everyonecancontribute/kubernetes | `#kubernetes` | - |
| everyonecancontribute/5-min-prod-app | `#5-min-prod-app` | [Setting](https://gitlab.com/groups/everyonecancontribute/5-min-prod-app/-/settings/integrations/discord/edit) |



