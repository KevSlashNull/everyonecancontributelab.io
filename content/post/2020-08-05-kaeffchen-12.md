---
Title: "12. Kaeffchen: Taming the Vector wolve and a LUA transform deep dive"
Date: 2020-08-05
Aliases: []
Tags: ["gitlab","devsecops","logs","vector","lua"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/22)
- Guests: Niclas Mietz, Carsten Köbke, Kevin Honka, Peter Rossbach, Bernhard Rausch, Michael Friedrich, Michael Aigner, Nico Meisenzahl, Greg Campion 
- Next ☕ chat `#13`: **2020-08-13** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/27)

22 members, 12 Kaeffchens, and so many ideas :-) 

### Highlights

- Niclas [tamed](https://twitter.com/vectordotdev/status/1291006267440287744) the wolve making it a nice live demo 
- Running [Vector](https://vector.dev/) and thinking about Loki with Tracing
- We could [help improve](https://twitter.com/dnsmichi/status/1291017814355083264) the Vector docs for Label key-value configuration ❤️
- Vector allows to use [LUA for transform hooks](https://vector.dev/docs/reference/transforms/lua/)
  - Change into embedding languages into other languages (Perl, Python, Lua, Rust, WebAssembly, etc.)
  - eBPF explanation: Kubernetes and [Cilium](https://cilium.io/)
- Thoughts on MFA, service users, ACME and webservers

### Recording

Enjoy the session!

{{< youtube q_ilzYTVr5M >}}

Here's the [intro](https://starwarsintrocreator.kassellabs.io/#!/CMDyr2mtW1ss1nUcDneUB).

### Insights

- Google Analytics alternatives:
  - https://usefathom.com/
  - https://matomo.org/ 
- Docs PR for Vector: https://github.com/timberio/vector/pull/3335/files  
- [Initial Lua integration in Vector](https://github.com/timberio/vector/issues/61)
- Vector daemon user permission problem, [shared on Twitter](https://twitter.com/dnsmichi/status/1291043615230353409)
  - [Open Policy Agent](https://www.openpolicyagent.org/): https://everyonecancontribute.com/post/2020-06-23-opa/ 
- [WebAssembly WASI](https://github.com/WebAssembly/WASI)  
- [Embedded Perl in C++ - technical deep dive](https://github.com/Icinga/icinga2/issues/7462)
- [Create your own full-text search engine](https://artem.krylysov.com/blog/2020/07/28/lets-build-a-full-text-search-engine/)
- MFA
  - [1.5 factor auth with 1Password](https://twitter.com/dnsmichi/status/1241481541227855876)
- [GitLab Service Users/Bots](https://gitlab.com/gitlab-org/gitlab/-/issues/6883)
- ACME and Certificates (topic pitch for next time)
  - https://smallstep.com/certificates/
  - https://caddyserver.com/   


### News

- [An inside look at the Rust programming language](https://about.gitlab.com/blog/2020/07/21/rust-programming-language/)
- [Understand Kubernetes terminology from namespaces to pods](https://about.gitlab.com/blog/2020/07/30/kubernetes-terminology/)
- [Boosting your kubectl productivity](https://twitter.com/k_irandoust/status/1288759726562512896)
- [Not putting logs into Elasticsearch but from it by Philipp Krenn](https://twitter.com/xeraa/status/1288906630365679617)
- [Monitoring demystified: A guide for logging, tracing, metrics](https://techbeacon.com/enterprise-it/monitoring-demystified-guide-logging-tracing-metrics)
- [How to use Prometheus for anomaly detection in GitLab](https://about.gitlab.com/blog/2019/07/23/anomaly-detection-using-prometheus/) 
- [GitLab Workflow for VS Code now with more Official](https://about.gitlab.com/blog/2020/07/31/use-gitlab-with-vscode/)
- [The road to Gitaly v1.0 (aka, why GitLab doesn't require NFS for storing Git data anymore)](https://about.gitlab.com/blog/2018/09/12/the-road-to-gitaly-1-0/)
- [DM on GitLab Slack: I need your DNS skills!](https://dnsmichi.at/2020/08/03/dm-on-gitlab-slack-i-need-your-dns-skills/)
- [10 most common mistakes using kubernetes](https://blog.pipetail.io/posts/2020-05-04-most-common-mistakes-k8s/)
- [Deploying GitLab to Anthos GKE on-prem to configure and run a CI/CD pipeline](https://cloud.google.com/solutions/partners/deploying-gitlab-anthos-gke-on-prem-cicd-pipeline)

### Bookmarks

- [Move the Three Pillars of Observability (+Alerting) to Core](https://gitlab.com/groups/gitlab-org/-/epics/2310)
- [Learning Git Twitter thread by Emma Bostian](https://twitter.com/EmmaBostian/status/1288801977510633477)
  - [Git/GitLab training created by Michael Friedrich](https://twitter.com/BrianLinuxing/status/1288834359764619264)
  - [A Hacker's Guide to Git](https://wildlyinaccurate.com/a-hackers-guide-to-git/) 
  - [Getting Git](https://gettinggit.com/)
  - [Git for ages 4 and up](https://www.youtube.com/watch?v=1ffBJ4sVUb4)
  - [Learn Git Branching](https://learngitbranching.js.org/)


